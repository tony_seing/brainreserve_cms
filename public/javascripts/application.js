// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults


  $(document).ready(function() {
 
    //Default
   // $("#slow_scroll").anchorScroll();
 
    //Custom FX
    $('.slow_scroll').anchorScroll({fx: "easeOutBounce"});


    $('a#link_what_we_do').hover(function() { $('img#what_we_do_line_highlighted').show(); },  function() { $('img#what_we_do_line_highlighted').hide(); });
    
    $('a#link_culture_pulse').hover(function() { $('img#culture_pulse_blog_line_highlighted').show(); },  function() { $('img#culture_pulse_blog_line_highlighted').hide(); });

    $('a#link_trend_bank').hover(function() { $('img#trend_bank_line_highlighted').show(); },  function() { $('img#trend_bank_line_highlighted').hide(); });

    $('a#link_about_us').hover(function() { $('img#about_us_line_highlighted').show(); },  function() { $('img#about_us_line_highlighted').hide(); });

    $('div#link1').hover(function() { $('div#main_left img.left_image').hide(); $('img#home_image_left_1').show(); $('')  });
    $('div#link2').hover(function() { $('div#main_left img.left_image').hide(); $('img#home_image_left_2').show();  });
    $('div#link3').hover(function() { $('div#main_left  img.left_image').hide(); $('img#home_image_left_3').show();  });
    $('div#link4').hover(function() { $('div#main_left  img.left_image').hide(); $('img#home_image_left_4').show();  });

    $('div#member1').hover(function() { $('table td#talentbank_right img, div#talentbank_right_right_text span').hide(); $('#bio_picture1').show(); $('#bio_text1').show();   });
    $('div#member2').hover(function() { $('table td#talentbank_right img, div#talentbank_right_right_text span').hide(); $('#bio_picture2').show(); $('#bio_text2').show(); });
    $('div#member3').hover(function() { $('table td#talentbank_right img, div#talentbank_right_right_text span').hide(); $('#bio_picture3').show(); $('#bio_text3').show(); });
    $('div#member4').hover(function() { $('table td#talentbank_right img, div#talentbank_right_right_text span').hide(); $('#bio_picture4').show(); $('#bio_text4').show(); });
    $('div#member5').hover(function() { $('table td#talentbank_right img, div#talentbank_right_right_text span').hide(); $('#bio_picture5').show(); $('#bio_text5').show(); });
    $('div#member6').hover(function() { $('table td#talentbank_right img, div#talentbank_right_right_text span').hide(); $('#bio_picture6').show(); $('#bio_text6').show(); });
    $('div#member7').hover(function() { $('table td#talentbank_right img, div#talentbank_right_right_text span').hide(); $('#bio_picture7').show(); $('#bio_text7').show(); });

    /*
    $('table#what_we_do_table td').click( function()
    {
       window.location = $(this).attr('src');

      
    });
    */
    
    
    // if document has a an anchor in it, adjust height
    //$(window).offset({ top: 230});
    
    $('div#trendbank_bottom td').click( function () {
      window.location = $(this).attr('destination');
      
    });
    
    $('div#trendbank_bottom td').hover( function () {
       $('#trendbank_description').html($('div#trendbank_bottom td').attr('description'));
      
    });
    
    $('div#meet_the_team table td').hover(
      function() {
	$(this).find('div.jobtitle_text').show();    
    }, 
      function() {
	$(this).find('div.jobtitle_text').hide();    
    });

    $('div#meet_the_team table td').click(
	function () {
	  $(location).attr('href',$(this).attr('href'));
	  
	}
    );

  });