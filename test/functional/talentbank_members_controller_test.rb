require 'test_helper'

class TalentbankMembersControllerTest < ActionController::TestCase
  setup do
    @talentbank_member = talentbank_members(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:talentbank_members)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create talentbank_member" do
    assert_difference('TalentbankMember.count') do
      post :create, :talentbank_member => @talentbank_member.attributes
    end

    assert_redirected_to talentbank_member_path(assigns(:talentbank_member))
  end

  test "should show talentbank_member" do
    get :show, :id => @talentbank_member.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @talentbank_member.to_param
    assert_response :success
  end

  test "should update talentbank_member" do
    put :update, :id => @talentbank_member.to_param, :talentbank_member => @talentbank_member.attributes
    assert_redirected_to talentbank_member_path(assigns(:talentbank_member))
  end

  test "should destroy talentbank_member" do
    assert_difference('TalentbankMember.count', -1) do
      delete :destroy, :id => @talentbank_member.to_param
    end

    assert_redirected_to talentbank_members_path
  end
end
