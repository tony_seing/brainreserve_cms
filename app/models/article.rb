class Article
  include Mongoid::Document
  include Mongoid::Paperclip
  include Mongoid::Timestamps
  include Mongoid::FullTextSearch

  
  field :name, :type => String
  field :description, :type => String
  field :description2, :type => String
  
  field :content, :type => String
  field :category, :type => String
  field :is_featured, :type => Boolean, :default => false
  field :the_date, :type => DateTime
  embeds_many :comments
  has_mongoid_attached_file :article_photo
  has_mongoid_attached_file :link_photo
  validates :name, :uniqueness => true
  validates :description, :presence => true
  validates :content, :presence => true
  
  fulltext_search_in :name, :description, :description2, :content	

end
