class Page
  include Mongoid::Document
include Mongoid::Timestamps
  field :name, :type => String
  field :description, :type => String
  embeds_many :snippets
  references_many :assets
end
