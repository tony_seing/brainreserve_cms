class Asset
  include Mongoid::Document
  include Mongoid::Paperclip

  field :name, :type => String
  field :description, :type => String
  has_mongoid_attached_file :asset_file
  referenced_in :page
  
  validates :name, :presence => true
  
end
