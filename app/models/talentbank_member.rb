class TalentbankMember
  include Mongoid::Document
   include Mongoid::Paperclip
include Mongoid::Timestamps
  field :name, :type => String
  field :email, :type => String
  has_mongoid_attached_file :profile_picture
  
  field :is_featured, :type => Boolean
  field :biography, :type => String
end
