class Snippet
  include Mongoid::Document

  embedded_in :page
  field :name, :type => String
  field :description, :type => String
  
  field :content, :type => String
  
end
