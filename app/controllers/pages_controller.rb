class PagesController < ApplicationController
  before_filter :authenticate_administrator!, :except => [:home, :show_article, :culturepulse, :culturepulse_article]
  
  # GET /pages
  # GET /pages.xml
  def index
    @pages = Page.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @pages }
    end
  end

  # GET /pages/1
  # GET /pages/1.xml
  def show
    @page = Page.find(params[:id])
    @snippets = @page.snippets.all
    @snippets = @snippets.paginate(:page => params[:page], :per_page => 10)
    @assets = @page.assets.all
    @assets = @page.assets.paginate(:page => params[:page], :per_page => 10)
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @page }
    end
  end
  
  
  def home
    
    #@page = Page.find(params[:id])
    #@snippets = @page.snippets.all
    #@snippets = @snippets.paginate(:page => params[:page], :per_page => 10)
    #@assets = @page.assets.all
    #@assets = @page.assets.paginate(:page => params[:page], :per_page => 10)
    
    @articles = Article.where(category: 'home').and(is_featured: true).order_by([ :created_at, :asc])    
    @what_we_do_articles = Article.where(category: 'what_we_do').and(is_featured: true).order_by([ :created_at, :asc])    
    @about_us_articles = Article.where(category: 'about_us').and(is_featured: true).order_by([ :created_at, :asc])    
    @clients_and_case_studies_articles = Article.where(category: 'clients_and_case_studies').and(is_featured: true).order_by([ :created_at, :asc])    
    @trendbank_articles = Article.where(category: 'trendbank').and(is_featured: true).order_by([ :created_at, :asc])   
    @talentbank_articles = Article.where(category: 'talentbank').and(is_featured: true).order_by([ :created_at, :asc])     

    @employees = Article.where(category: 'employee').and(is_featured: true).order_by([ :created_at, :asc])   

   
    respond_to do |format|
      format.html { render  :layout => 'cosmos'} # show.html.erb
      format.xml  { render :xml => @page }
    end
  end

  def culturepulse
    
    #@page = Page.find(params[:id])
    #@snippets = @page.snippets.all
    #@snippets = @snippets.paginate(:page => params[:page], :per_page => 10)
    #@assets = @page.assets.all
    #@assets = @page.assets.paginate(:page => params[:page], :per_page => 10)
    
    @articles = Article.where(category: 'culture_pulse_blog').and(is_featured: true).order_by([:the_date]).limit(7) 
    
    
    respond_to do |format|
      format.html { render  :layout => false} # show.html.erb
      format.xml  { render :xml => @page }
    end
  end
  
  def culturepulse_search
    
    @articles = Article.fulltext_search(params[:field], :filters => { :category => 'culture_pulse_blog'} )
    respond_to do |format|
      format.html { render  :layout => false} # show.html.erb
      format.xml  { render :xml => @page }
    end
  end

  def culturepulse_article
    
    #@page = Page.find(params[:id])
    #@snippets = @page.snippets.all
    #@snippets = @snippets.paginate(:page => params[:page], :per_page => 10)
    #@assets = @page.assets.all
    #@assets = @page.assets.paginate(:page => params[:page], :per_page => 10)
    
    @articles = Article.where(category: 'culture_pulse_blog').and(is_featured: true).and(name: params[:name]) 
    
    
    respond_to do |format|
      format.html { render  :layout => false} # show.html.erb
      format.xml  { render :xml => @page }
    end
  end
  
  def show_article
    
    #@page = Page.find(params[:id])
    #@snippets = @page.snippets.all
    #@snippets = @snippets.paginate(:page => params[:page], :per_page => 10)
    #@assets = @page.assets.all
    #@assets = @page.assets.paginate(:page => params[:page], :per_page => 10)
    
    @article = Article.where(name: params[:name])
   #@new_comment = @article[0].comments.new if @article[0].comments.present?
    @comments = @article[0].comments.all if @article[0].present?
    respond_to do |format|
      format.html { render  :layout => 'article'} # show.html.erb
      format.xml  { render :xml => @page }
    end
  end

  # GET /pages/new
  # GET /pages/new.xml
  def new
    @page = Page.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @page }
    end
  end

  # GET /pages/1/edit
  def edit
    @page = Page.find(params[:id])
  end

  # POST /pages
  # POST /pages.xml
  def create
    @page = Page.new(params[:page])

    respond_to do |format|
      if @page.save
        format.html { redirect_to(@page, :notice => 'Page was successfully created.') }
        format.xml  { render :xml => @page, :status => :created, :location => @page }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @page.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /pages/1
  # PUT /pages/1.xml
  def update
    @page = Page.find(params[:id])

    respond_to do |format|
      if @page.update_attributes(params[:page])
        format.html { redirect_to(@page, :notice => 'Page was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @page.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.xml
  def destroy
    @page = Page.find(params[:id])
    @page.destroy

    respond_to do |format|
      format.html { redirect_to(pages_url) }
      format.xml  { head :ok }
    end
  end
end
