class TalentbankMembersController < ApplicationController
  # GET /talentbank_members
  # GET /talentbank_members.xml
  def index
    @talentbank_members = TalentbankMember.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @talentbank_members }
    end
  end

  # GET /talentbank_members/1
  # GET /talentbank_members/1.xml
  def show
    @talentbank_member = TalentbankMember.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @talentbank_member }
    end
  end

  # GET /talentbank_members/new
  # GET /talentbank_members/new.xml
  def new
    @talentbank_member = TalentbankMember.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @talentbank_member }
    end
  end

  # GET /talentbank_members/1/edit
  def edit
    @talentbank_member = TalentbankMember.find(params[:id])
  end

  # POST /talentbank_members
  # POST /talentbank_members.xml
  def create
    @talentbank_member = TalentbankMember.new(params[:talentbank_member])
 
    respond_to do |format|
      if @talentbank_member.save
        format.html { redirect_to(talentbank_members_url, :notice => 'Talentbank member was successfully created.') }
        format.xml  { render :xml => @talentbank_member, :status => :created, :location => @talentbank_member }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @talentbank_member.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /talentbank_members/1
  # PUT /talentbank_members/1.xml
  def update
    @talentbank_member = TalentbankMember.find(params[:id])

    respond_to do |format|
      if @talentbank_member.update_attributes(params[:talentbank_member])
        format.html { redirect_to(talentbank_members_url, :notice => 'Talentbank member was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @talentbank_member.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /talentbank_members/1
  # DELETE /talentbank_members/1.xml
  def destroy
    @talentbank_member = TalentbankMember.find(params[:id])
    @talentbank_member.destroy

    respond_to do |format|
      format.html { redirect_to(talentbank_members_url) }
      format.xml  { head :ok }
    end
  end
end
