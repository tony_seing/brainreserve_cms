class CommentsController < ApplicationController
  # GET /comments
  # GET /comments.xml
  def index
     @article =  Article.find(params[:article_id])
  
    @comments = @article.comments.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.xml
  def show
     @article =  Article.find(params[:article_id])
  
    @comment = @article.comments.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.xml
  def new
     @article =  Article.find(params[:article_id])
  
    @comment = @article.comments.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @comment }
    end
  end

  # GET /comments/1/edit
  def edit
     @article =  Article.find(params[:article_id])
  
    @comment = @article.comments.find(params[:id])
  end

  # POST /comments
  # POST /comments.xml
  def create
    @article =  Article.find(params[:article_id])
    @comment = @article.comments.new(params[:comment])

    respond_to do |format|
      if @comment.save
        format.html { redirect_to(article_comments_url(@article), :notice => 'Comment was successfully created.') }
        format.xml  { render :xml => @comment, :status => :created, :location => @comment }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @comment.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.xml
  def update
    @article =  Article.find(params[:article_id])
    @comment = @article.comments.find(params[:comment])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to(article_comments_url(@article), :notice => 'Comment was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @comment.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.xml
  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to(article_comments_url(@article)) }
      format.xml  { head :ok }
    end
  end
end
