class SnippetsController < ApplicationController
   before_filter :authenticate_administrator!
  # GET /snippets
  # GET /snippets.xml
  def index
    @page = Page.find(params[:page_id])
    @snippets = @page.snippets.all
    @snippets = @snippets.paginate(:page => params[:page], :per_page => 10)
    

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @snippets }
    end
  end

  # GET /snippets/1
  # GET /snippets/1.xml
  def show
    @page = Page.find(params[:page_id])
    @snippet = @page.snippets.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @snippet }
    end
  end

  # GET /snippets/new
  # GET /snippets/new.xml
  def new
    @page = Page.find(params[:page_id])
    @snippet = @page.snippets.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @snippet }
    end
  end

  # GET /snippets/1/edit
  def edit
     @page = Page.find(params[:page_id])
  
    @snippet = @page.snippets.find(params[:id])
  end

  # POST /snippets
  # POST /snippets.xml
  def create
     @page = Page.find(params[:page_id])
  
    @snippet = @page.snippets.new(params[:snippet])

    respond_to do |format|
      if @snippet.save
        format.html { redirect_to(page_path(@page), :notice => 'Snippet was successfully created.') }
        format.xml  { render :xml => @snippet, :status => :created, :location => @snippet }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @snippet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /snippets/1
  # PUT /snippets/1.xml
  def update
      @page = Page.find(params[:page_id])
    @snippet = @page.snippets.find(params[:id])

    respond_to do |format|
      if @snippet.update_attributes(params[:snippet])
        format.html { redirect_to(page_path(@page), :notice => 'Snippet was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @snippet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /snippets/1
  # DELETE /snippets/1.xml
  def destroy
    @page = Page.find(params[:page_id])
    @snippet = @page.snippets.find(params[:id])
    @snippet.destroy

    respond_to do |format|
      format.html { redirect_to(page_url(@page)) }
      format.xml  { head :ok }
    end
  end
end
