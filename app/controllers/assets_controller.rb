class AssetsController < ApplicationController
   before_filter :authenticate_administrator!
  
  
  
  
  # GET /assets
  # GET /assets.xml
  def index
    @page = Page.find(params[:page_id])
    @assets = @page.assets.all
    @assets = @assets.paginate(:page => params[:page], :per_page => 1)
   
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @assets }
    end
  end

  # GET /assets/1
  # GET /assets/1.xml
  def show
      @page = Page.find(params[:page_id])
    @asset = @page.assets.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @asset }
    end
  end

  # GET /assets/new
  # GET /assets/new.xml
  def new
      @page = Page.find(params[:page_id])
    @asset = @page.assets.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @asset }
    end
  end

  # GET /assets/1/edit
  def edit
      @page = Page.find(params[:page_id])
    @asset = @page.assets.find(params[:id])
  end

  # POST /assets
  # POST /assets.xml
  def create
      @page = Page.find(params[:page_id])
    @asset = @page.assets.new(params[:asset])

    respond_to do |format|
      if @asset.save
        format.html { redirect_to(page_url(@page), :notice => 'Asset was successfully created.') }
        format.xml  { render :xml => @asset, :status => :created, :location => @asset }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @asset.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /assets/1
  # PUT /assets/1.xml
  def update
      @page = Page.find(params[:page_id])
    @asset = @page.assets.find(params[:id])

    respond_to do |format|
      if @asset.update_attributes(params[:asset])
        format.html { redirect_to(page_url(@page), :notice => 'Asset was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @asset.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /assets/1
  # DELETE /assets/1.xml
  def destroy
    @page = Page.find(params[:page_id])
    @asset = @page.assets.find(params[:id])
    @asset.destroy

    respond_to do |format|
      format.html { redirect_to(page_url(@page)) }
      format.xml  { head :ok }
    end
  end
end
